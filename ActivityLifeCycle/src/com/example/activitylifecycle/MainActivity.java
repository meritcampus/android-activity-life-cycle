package com.example.activitylifecycle;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends Activity {
	static final String TAG = " LifeCycle ";

	// You will not see any output on the emulator or device.You need to open
	// logcat.

	/*
	 * When the Activity first time loads the events are called as below:
	 * onCreate(),onStart(),onResume() entire life cycle oncreate will call only
	 * once
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Log.d(TAG, "called oncreate method");
	}

	@Override
	protected void onStart() {
		super.onStart();
		Log.d(TAG, "called onStart method");
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.d(TAG, "called oResume method");
	}

	/*
	 * When you click on Home button the Activity goes to the background and the
	 * below events are called:
	 * onPause() onStop()
	 */
	@Override
	protected void onPause() {
		super.onPause();
		Log.d(TAG, "called onPause method");
	}

	@Override
	protected void onStop() {
		super.onStop();
		Log.d(TAG, "called onStop method");
	}

	/*
	 * When you click the back button OR try to finish() the activity the events
	 * are called as below:
	 * onPause() ,onStop(), onDestroy()
	 */
	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "called onDestroy method");
	}

}
